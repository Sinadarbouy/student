﻿using IdentitySample.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Student.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace IdentitySample.Controllers
{
    [Authorize]
    public class ManageController : Controller
    {
        CoconutEntities DB = new CoconutEntities();
        public ManageController()
        {
        }

        public ManageController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Index
        [HttpGet]
        public async Task<ActionResult> Index(ManageMessageId? message)
        {
        
            var userId = User.Identity.GetUserId();
         

            var Model =  DB.Classes.Where(x => x.UserID == userId).ToList();
            var joinedClasses = DB.ClassUsers.Where(x => x.UserID == userId).ToList();

            ViewBag.joined = joinedClasses;

            ViewBag.ReturnUrl = "/Manage/index";
            return View(Model);
        }

        [HttpGet]
        public async Task<ActionResult> classchat(int classID)
        {
            var model =DB.Messages.Where(x=>x.ClassID == classID).ToList();
            ViewBag.classid = classID;
            return View(model);
        }

        //allClasses
        [HttpGet]
        public async Task<ActionResult> allClasses()
        {
            var userId = User.Identity.GetUserId();
            List<Class> Model = new List<Class>();
            var allclass = DB.Classes.Where(x => x.UserID != userId).ToList();

            foreach (var item in allclass)
            {
                if (!item.ClassUsers.Where(x => x.UserID == userId).Any()) {

                    Model.Add(item);
                
                }
            }
           
            ViewBag.ReturnUrl = "/Manage/allClasses";
            return View(Model);
        }


        //classowner
        [HttpGet]
        public async Task<ActionResult> ClassOwner(int ClassID)
        {
            var userId = User.Identity.GetUserId();

            ViewBag.classid = ClassID;
            var tests = DB.Testes.Where(x => x.classID == ClassID && x.Class.UserID == userId).ToList();
            var student = DB.ClassUsers.Where(x => x.ClassID == ClassID && x.UserID != userId).ToList();
            ViewBag.students = student;
            ViewBag.ReturnUrl = "/Manage/ClassOwner";
            return View(tests);
        }

        [HttpPost]
        public async Task<ActionResult> ClassOwner(int ClassID,string testname)
        {
            var userId = User.Identity.GetUserId();

            ViewBag.classid = ClassID;
            var student = DB.ClassUsers.Where(x => x.ClassID == ClassID && x.UserID != userId).ToList();
            ViewBag.students = student;

            Teste test= new Teste()
            {
                classID=ClassID,
                Name = testname

            };
            DB.Testes.Add(test);
            DB.SaveChanges();
            ViewBag.ReturnUrl = "/Manage/ClassOwner";
            var tests = DB.Testes.Where(x => x.classID == ClassID && x.Class.UserID == userId).ToList();
            return View(tests);
        }

        [HttpPost]
        public string GetStudentByTestID(int testID)
        {
            var model = DB.Answers.Where(x => x.Question.testID == testID).Select(x => x.AspNetUser.UserName).Distinct().ToList();
            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
        }

        [HttpGet]
        public async Task<ActionResult> answers(string username,int testid)
        {
            var userId = User.Identity.GetUserId();

            var model = DB.Answers.Where(x => x.Question.testID == testid && x.AspNetUser.UserName == username).ToList();
            return View(model);
        }
        

        [HttpPost]
        public async Task<ActionResult> submitscore(List<int> score, List<int> answerid)
        {
            var userId = User.Identity.GetUserId();
            for (int i = 0; i < answerid.Count; i++)
            {
                var intanswerid = answerid[i];
                int score1 = score[i];
                Answer answer = DB.Answers.Find(intanswerid);
                answer.Score = score1;
            }
            DB.SaveChanges();
         
            return RedirectToAction("index");
        }

        //joinedClass
        [HttpGet]
        public async Task<ActionResult> studentClass(int ClassID)
        {
            var userId = User.Identity.GetUserId();

            ViewBag.classid = ClassID;
            var tests = DB.Testes.Where(x => x.classID == ClassID && x.Class.UserID != userId).ToList();
            ViewBag.ReturnUrl = "/Manage/studentClass";
            return View(tests);
        }

        //takeTest
        [HttpGet]
        public async Task<ActionResult> takeTest(int testId)
        {
            var userId = User.Identity.GetUserId();

            ViewBag.testId = testId;
            var model = DB.Questions.Where(x => x.testID == testId).OrderBy(x => x.Order).ToList();
            ViewBag.ReturnUrl = "/Manage/takeTest";
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> submitTest(List<int> questionId,List<string> answer)
        {
            var userId = User.Identity.GetUserId();

            for (int i = 0; i < answer.Count(); i++)
            {
                Answer objAnswer = new Answer()
                {
                    Text = answer[i],
                    QuestionID = questionId[i],
                    UserID = userId
                };
                DB.Answers.Add(objAnswer);
            }
            DB.SaveChanges();
            return RedirectToAction("index");
            
        }

        //createTest
        [HttpGet]
        public async Task<ActionResult> createTest(int testId)
        {

            ViewBag.testId = testId;
            var model = DB.Questions.Where(x => x.testID == testId).OrderBy(x => x.Order).ToList();
      
            
            ViewBag.ReturnUrl = "/Manage/createTest";
            return View(model);
  
        }

        [HttpPost]
        public async Task<ActionResult> createTest(int testId, string questionText)
        {
            var userId = User.Identity.GetUserId();

            ViewBag.testId = testId;
            var Questions = DB.Questions.Where(x => x.testID == testId).OrderBy(x => x.Order).ToList();
            int order = Questions.Count;
            Question question = new Question()
            {
                testID = testId,
                Title = questionText,
                Order = order + 1

            };
            DB.Questions.Add(question);
            DB.SaveChanges();
            ViewBag.ReturnUrl = "/Manage/createTest";
            var model = DB.Questions.Where(x => x.testID == testId).OrderBy(x => x.Order).ToList();
            return View(model);
        }

        //allClasses
        //[HttpGet]
        //public async Task<ActionResult> studentClass()
        //{
        //    var userId = User.Identity.GetUserId();

        //    var Model = DB.Classes.Where(x => x.UserID == userId).ToList();
        //    //var tests = DB.ClassTeste.Where(x => x.ClassID == ClassID);
        //    ViewBag.ReturnUrl = "/Manage/ClassOwner";
        //    return View(Model);
        //}
        //
        // GET: /Account/RemoveLogin
        [HttpGet]
        public ActionResult RemoveLogin()
        {
            var linkedAccounts = UserManager.GetLogins(User.Identity.GetUserId());
            ViewBag.ShowRemoveButton = HasPassword() || linkedAccounts.Count > 1;
            return View(linkedAccounts);
        }

        //
        // POST: /Manage/RemoveLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RemoveLogin(string loginProvider, string providerKey)
        {
            ManageMessageId? message;
            var userId = User.Identity.GetUserId();
            var result = await UserManager.RemoveLoginAsync(userId, new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(userId);
                if (user != null)
                {
                    await SignInAsync(user, isPersistent: false);
                }
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction("ManageLogins", new { Message = message });
        }

        //
        // GET: /Account/AddPhoneNumber
        [HttpGet]
        public ActionResult AddPhoneNumber()
        {
            return View();
        }

        //
        // POST: /Account/AddPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddPhoneNumber(AddPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            // Generate the token and send it
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), model.Number);
            if (UserManager.SmsService != null)
            {
                var message = new IdentityMessage
                {
                    Destination = model.Number,
                    Body = "Your security code is: " + code
                };
                await UserManager.SmsService.SendAsync(message);
            }
            return RedirectToAction("VerifyPhoneNumber", new { PhoneNumber = model.Number });
        }

        //
        // POST: /Manage/RememberBrowser
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RememberBrowser()
        {
            var rememberBrowserIdentity = AuthenticationManager.CreateTwoFactorRememberBrowserIdentity(User.Identity.GetUserId());
            AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = true }, rememberBrowserIdentity);
            return RedirectToAction("Index", "Manage");
        }

        //
        // POST: /Manage/ForgetBrowser
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ForgetBrowser()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);
            return RedirectToAction("Index", "Manage");
        }

        //
        // POST: /Manage/EnableTFA
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EnableTFA()
        {
            var userId = User.Identity.GetUserId();
            await UserManager.SetTwoFactorEnabledAsync(userId, true);
            var user = await UserManager.FindByIdAsync(userId);
            if (user != null)
            {
                await SignInAsync(user, isPersistent: false);
            }
            return RedirectToAction("Index", "Manage");
        }

        //
        // POST: /Manage/DisableTFA
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DisableTFA()
        {
            var userId = User.Identity.GetUserId();
            await UserManager.SetTwoFactorEnabledAsync(userId, false);
            var user = await UserManager.FindByIdAsync(userId);
            if (user != null)
            {
                await SignInAsync(user, isPersistent: false);
            }
            return RedirectToAction("Index", "Manage");
        }

        //
        // GET: /Account/VerifyPhoneNumber
        [HttpGet]
        public async Task<ActionResult> VerifyPhoneNumber(string phoneNumber)
        {
            // This code allows you exercise the flow without actually sending codes
            // For production use please register a SMS provider in IdentityConfig and generate a code here.
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), phoneNumber);
            ViewBag.Status = "For DEMO purposes only, the current code is " + code;
            return phoneNumber == null ? View("Error") : View(new VerifyPhoneNumberViewModel { PhoneNumber = phoneNumber });
        }

        //
        // POST: /Account/VerifyPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyPhoneNumber(VerifyPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var userId = User.Identity.GetUserId();
            var result = await UserManager.ChangePhoneNumberAsync(userId, model.PhoneNumber, model.Code);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(userId);
                if (user != null)
                {
                    await SignInAsync(user, isPersistent: false);
                }
                return RedirectToAction("Index", new { Message = ManageMessageId.AddPhoneSuccess });
            }
            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "Failed to verify phone");
            return View(model);
        }

        //
        // GET: /Account/RemovePhoneNumber
        [HttpGet]
        public async Task<ActionResult> RemovePhoneNumber()
        {
            var userId = User.Identity.GetUserId();
            var result = await UserManager.SetPhoneNumberAsync(userId, null);
            if (!result.Succeeded)
            {
                return RedirectToAction("Index", new { Message = ManageMessageId.Error });
            }
            var user = await UserManager.FindByIdAsync(userId);
            if (user != null)
            {
                await SignInAsync(user, isPersistent: false);
            }
            return RedirectToAction("Index", new { Message = ManageMessageId.RemovePhoneSuccess });
        }

        //
        // GET: /Manage/ChangePassword
        [HttpGet]
        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Account/Manage
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var userId = User.Identity.GetUserId();
            var result = await UserManager.ChangePasswordAsync(userId, model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(userId);
                if (user != null)
                {
                    await SignInAsync(user, isPersistent: false);
                }
                return RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
            }
            AddErrors(result);
            return View(model);
        }

        //
        // GET: /Manage/SetPassword
        [HttpGet]
        public ActionResult SetPassword()
        {
            return View();
        }

        //
        // POST: /Manage/SetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SetPassword(SetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userId = User.Identity.GetUserId();
                var result = await UserManager.AddPasswordAsync(userId, model.NewPassword);
                if (result.Succeeded)
                {
                    var user = await UserManager.FindByIdAsync(userId);
                    if (user != null)
                    {
                        await SignInAsync(user, isPersistent: false);
                    }
                    return RedirectToAction("Index", new { Message = ManageMessageId.SetPasswordSuccess });
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/Manage
        public async Task<ActionResult> ManageLogins(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            var userId = User.Identity.GetUserId();
            var user = await UserManager.FindByIdAsync(userId);
            if (user == null)
            {
                return View("Error");
            }
            var userLogins = await UserManager.GetLoginsAsync(userId);
            var otherLogins = AuthenticationManager.GetExternalAuthenticationTypes().Where(auth => userLogins.All(ul => auth.AuthenticationType != ul.LoginProvider)).ToList();
            ViewBag.ShowRemoveButton = user.PasswordHash != null || userLogins.Count > 1;
            return View(new ManageLoginsViewModel
            {
                CurrentLogins = userLogins,
                OtherLogins = otherLogins
            });
        }

        //
        // POST: /Manage/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new AccountController.ChallengeResult(provider, Url.Action("LinkLoginCallback", "Manage"), User.Identity.GetUserId());
        }

        //
        // GET: /Manage/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
            var userId = User.Identity.GetUserId();
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, userId);
            if (loginInfo == null)
            {
                return RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
            }
            var result = await UserManager.AddLoginAsync(userId, loginInfo.Login);
            return result.Succeeded ? RedirectToAction("ManageLogins") : RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
        }


        // POST: /Manage/DisableTFA
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddClass(string title,string about, string returnUrl)
        {
            var userId = User.Identity.GetUserId();

            Class objclass = new Class()
            {
                About = about,
                UserID = userId,
                Name = title
            };

            DB.Classes.Add(objclass);
            DB.SaveChanges();
            return RedirectToLocal(returnUrl);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> JoinClass(string classid)
        {
            var userId = User.Identity.GetUserId();

            ClassUser classUser = new ClassUser()
            {
                UserID = userId,
                ClassID = int.Parse(classid)
            };
            DB.ClassUsers.Add(classUser);
            DB.SaveChanges();
            return RedirectToAction("allclasses");
        }
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }


        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie, DefaultAuthenticationTypes.TwoFactorCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = isPersistent }, await user.GenerateUserIdentityAsync(UserManager));
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        private bool HasPhoneNumber()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PhoneNumber != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }

        #endregion
    }
}