﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Student.Models;
using IdentitySample.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;


namespace Student.Hubs
{
    public class MyHub : Hub
    {
       


        CoconutEntities DB = new CoconutEntities();

        //-->>>>> ***** Receive Request From Client [  Connect  ] *****
        public void Connect(string UserID,string classID)
        {
            var id = Context.ConnectionId;
          

            var userInfo = DB.AspNetUsers.Find(UserID);



            try
            {

                    Groups.Add(Context.ConnectionId, classID);
                    Clients.Caller.onConnected(id, userInfo.UserName, 1, classID);

            }

            catch
            {
                string msg = "All Administrators are busy, please be patient and try again";
                //***** Return to Client *****
                Clients.Caller.NoExistAdmin();

            }


        }
        // <<<<<-- ***** Return to Client [  NoExist  ] *****



        //--group ***** Receive Request From Client [  SendMessageToGroup  ] *****
        public void SendMessageToGroup(string userName, string message,string classID)
        {

         
          
                //***** Return to Client *****
                Clients.Group(classID).getMessages(userName, message);
                var user = DB.AspNetUsers.Where(x=>x.UserName == userName).FirstOrDefault();
                Message messagebody = new Message()
                {
                    datetime = DateTime.Now,
                    Message1 = message,
                    ClassID=int.Parse(classID),
                    UserID=user.Id
                    

                };
                DB.Messages.Add(messagebody);
                DB.SaveChanges();
            

        }

    }
}